﻿using UnityEngine;
using System.Collections;

public class N3KWalking : BaseState
{
	public override void Construct ()
	{
		base.Construct ();
		motor.VerticalVelocity = 0.0f;
        motor.AirExhausted = false;
	}

	public override Vector3 ProcessMotion (Vector3 input)
	{
		MotorHelper.KillVector (ref input, motor.WallVector);
		MotorHelper.FollowVector (ref input,motor.SlopeNormal);
        MotorHelper.ApplySpeed(ref input, motor.Speed);

        return input;
	}

	public override void PlayerTransition()
	{
		base.PlayerTransition();

		if (!motor.Grounded ())
			motor.ChangeState ("N3KFalling");

		if (InputManager.IsDown(InputName.A))
			motor.ChangeState ("N3KJumping");

        if (Input.GetKeyDown(KeyCode.LeftShift))
            motor.ChangeState("N3K_WM_GrapplingHook");
	}
}