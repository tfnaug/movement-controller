﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class N3K_WM_EndGrapplingHook : BaseState
{
    private Vector3 hookPoint;
    private Vector3 hookNormal;

    public override void Construct()
    {
        base.Construct();

        motor.VerticalVelocity = 2.0f;
        hookPoint = motor.hookPoint;
        hookNormal = motor.hookNormal;
    }

    public override Vector3 ProcessMotion(Vector3 input)
    {
        // Get the direction in between you and the hookpoint
        input = -hookNormal * motor.Speed;

        return input;
    }

    public override void PlayerTransition()
    {
        base.PlayerTransition();

        motor.ChangeState("N3KWalking");
    }
}
